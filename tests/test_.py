import datetime
import time
from datetime import date

import pytest
from fastapi.testclient import TestClient

from employee_service.auth import gen_password_hash
from employee_service.models import Employee
from employee_service.server import make_app
from employee_service.storage import InMemoryStorage

storage = InMemoryStorage(
    users=[
        Employee(
            username="user1",
            salary=1000,
            salary_increase_date=date.fromisoformat("2023-12-12"),
        ),
        Employee(
            username="user2",
            salary=2000,
            salary_increase_date=date.fromisoformat("2024-10-10"),
        ),
    ],
    password_hashes={
        "user1": gen_password_hash("password1"),
        "user2": gen_password_hash("password2"),
    },
)
jwt_secret = "test_jwt_hs256_secret_key"
jwt_validity_time = datetime.timedelta(seconds=3)
client = TestClient(make_app(storage, jwt_secret, jwt_validity_time))


def test_get_salary():
    response = client.post(
        "/login", json={"username": "user1", "password": "password1"}
    )
    token = response.json()["access_token"]
    response = client.get("/users/salary", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200
    assert response.json() == {"salary": 1000, "salary_increase_date": "2023-12-12"}


@pytest.mark.parametrize(
    "username,password",
    [
        ("user1", "password2"),  # incorrect password
        ("user3", "password2"),  # non-existing user
    ],
)
def test_login_invalid_credentials(username, password):
    response = client.post("/login", json={"username": username, "password": password})
    assert response.status_code == 401
    assert response.text == ""


def test_get_salary_invalid_token():
    response = client.get(
        "/users/salary", headers={"Authorization": f"Bearer invalid_token"}
    )
    assert response.status_code == 401
    assert response.text == ""


def test_get_salary_expired_token():
    token = client.post(
        "/login", json={"username": "user1", "password": "password1"}
    ).json()["access_token"]
    time.sleep(jwt_validity_time.total_seconds() + 1)
    response = client.get("/users/salary", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 401
    assert response.json() == {"message": "Token expired"}
